//
//  HorizontalScroller.h
//  BlueLibrary
//
//  Created by caponate on 17/08/16.
//  Copyright © 2016 Eli Ganem. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HorizontalScrollerDelegate;
@interface HorizontalScroller : UIView

@property (weak) id<HorizontalScrollerDelegate> delegate;
- (void)reload;

@end

@protocol HorizontalScrollerDelegate <NSObject>

@required

//el delegado pregunta cuantas vistas quieres dibujar dentro del HorizontalScroller
- (NSInteger)numberOfViewsForHorizontalScroller:(HorizontalScroller*)scroller;

// Recoge la vista donde se dibuja el <index>
-(UIView*)horizontalScroller:(HorizontalScroller*)scroller viewAtIndex:(int)index;

// informa al delegado que <index> ha sido clicado
-(void)horizontalScroller:(HorizontalScroller*)scroller clickedViewAtIndex:(int)index;

@optional

// recoge cual es el <indice> que debe mostrar al principio

- (NSInteger)initialViewIndexForHorizontalScroller:(HorizontalScroller*)scroller;

@end