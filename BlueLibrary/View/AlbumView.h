//
//  AlbumView.h
//  BlueLibrary
//
//  Created by caponate on 11/08/16.
//  Copyright © 2016 DDura. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumView : UIView

- (id)initWithFrame:(CGRect)frame albumCover:(NSString*)albumCover;
@end
