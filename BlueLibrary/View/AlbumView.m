//
//  AlbumView.m
//  BlueLibrary
//
//  Created by caponate on 11/08/16.
//  Copyright © 2016 DDura. All rights reserved.
//

#import "AlbumView.h"

/**
 *  @author DDura, 16-08-11 09:08:52
 *
 *  Creamos estas variables de forma privada porque no hace falta 
 *  Esta conveción es estremadamente importante cuando estamos creando librerias o frameworks para otra gente.
 */

@implementation AlbumView{
    UIImageView *coverImage;
    UIActivityIndicatorView *indicator;
}


-(id)initWithFrame:(CGRect)frame albumCover:(NSString *)albumCover{
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor blackColor];
        coverImage = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, frame.size.width-10, frame.size.height-10)];
        
        [self addSubview:coverImage];
        
        indicator = [[UIActivityIndicatorView alloc]init];
        indicator.center = self.center;
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [indicator startAnimating];
        [self addSubview:indicator];
        
        /**
         *  KVO Patrón observador
         */
        [coverImage addObserver:self forKeyPath:@"image" options:0 context:nil];
        
        //Notificacion
        //Observer Pattern
        //GOAL::
        [[NSNotificationCenter defaultCenter]postNotificationName:@"BLDownloadImageNotification" object:self userInfo:@{@"imageView":coverImage,@"coverUrl":albumCover}];
    }
    
    return self;
}

-(void)dealloc{
    [coverImage removeObserver:self forKeyPath:@"image"];
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"image"]) {
        [indicator stopAnimating];
    }
}
@end
