//
//  Album+TableRepresentation.h
//  BlueLibrary
//
//  Created by caponate on 17/08/16.
//  Copyright © 2016 Eli Ganem. All rights reserved.
//

#import "Album.h"

@interface Album (TableRepresentation)

/**
 *  @author DDura, 16-08-17 09:08:18
 *
 *  Darse cuenta del tr_ en el principio del método, es la abreviación del nombre de la categoria!
 *  Previene colisiones con otros metodos
 *
 *
 */
-(NSDictionary *)tr_tableRepresentation;


@end
