//
//  LibraryAPI.m
//  BlueLibrary
//
//  Created by caponate on 11/08/16.
//  Copyright © 2016 DDura. All rights reserved.
//

#import "LibraryAPI.h"
#import "PersistencyManager.h"
#import "HTTPClient.h"




@interface LibraryAPI(){
    PersistencyManager *persistencyManager;
    HTTPClient *httpClient;
    BOOL isOnline; // Determina si el servidor debe ser actualizado con algún cambio
}

@end

@implementation LibraryAPI

+ (LibraryAPI*)sharedInstance{
    
    /**
     *  @author DDura, 16-08-11 10:08:48
     *
     *  Declaramos una variable estatica para guardar la istanci
     */
    static LibraryAPI *_sharedInstance = nil;
    
    /**
     *  @author DDura, 16-08-11 10:08:24
     *
     *  Declaramos una variable estatica dispatch_once_t que nos asegura que la inacialización se ejecuta una única vez.
     */
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[LibraryAPI alloc]init];
        
    });
    return _sharedInstance;
}


- (id)init{
    self = [super init];
    if (self) {
        persistencyManager  = [[PersistencyManager alloc]init];
        httpClient          = [[HTTPClient alloc]init];
        isOnline            = NO;
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(downloadImage:) name:@"BLDownloadImageNotification" object:nil];
    }
    return self;
}

// Es importante eliminar el observador del objeto cuando el objeto ya no está en memoria
// si no, puede crashear la app

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)downloadImage:(NSNotification*)notification{
    UIImageView *imageView = notification.userInfo[@"imageView"];
    NSString    *coverUrl = notification.userInfo[@"coverUrl"];
    
    imageView.image = [persistencyManager getImage:[coverUrl lastPathComponent]];
    
    if (imageView.image == nil) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImage *image = [httpClient downloadImage:coverUrl];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                imageView.image = image;
                [persistencyManager saveImage:image fileName:[coverUrl lastPathComponent]];
            });
        });
    }
}

- (NSArray *)getAlbums{
    return [persistencyManager getAlbums];
}

- (void)saveAlbums{
    [persistencyManager saveAlbums];
}

- (void)addAlbum:(Album *)album atIndex:(int)index{
    [persistencyManager addAlbum:album atIndex:index];
    if (isOnline) {
        [httpClient postRequest:@"/api/addAlbum" body:[album description]];
    }

}

- (void)deleteAlbumAtIndex:(int)index{
    [persistencyManager deleteAlbumAtIndex:index];
    if (isOnline) {
        [httpClient postRequest:@"/api/deleteAlbum" body:[@(index) description]];
    }
}



@end
