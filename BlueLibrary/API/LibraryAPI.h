//
//  LibraryAPI.h
//  BlueLibrary
//
//  Created by caponate on 11/08/16.
//  Copyright © 2016 DDura. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Album.h"

@interface LibraryAPI : NSObject


+(LibraryAPI*)sharedInstance;


- (NSArray*)getAlbums;
- (void)saveAlbums;
- (void)addAlbum:(Album *)album atIndex:(int)index;
- (void)deleteAlbumAtIndex:(int)index;

@end