//
//  PersistencyManager.m
//  BlueLibrary
//
//  Created by caponate on 11/08/16.
//  Copyright © 2016 DDura. All rights reserved.
//

#import "PersistencyManager.h"

@interface PersistencyManager(){
    NSMutableArray *albums;
}

@end

@implementation PersistencyManager

- (id)init{
    self = [super init];
    if (self) {
        NSData *data = [NSData dataWithContentsOfFile:[NSHomeDirectory()stringByAppendingString:@"/Documents/albums.bin"]];
        
        albums = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if (albums == nil) {
            
        albums = [NSMutableArray arrayWithArray:@[[[Album alloc] initWithTitle:@"Best of Bowie" artist:@"David Bowie" coverUrl:@"http://cdn-ak.f.st-hatena.com/images/fotolife/S/SASAKI-FUJIWORI/20160115/20160115192730.jpg" year:@"1992"],
                                                  [[Album alloc] initWithTitle:@"It's My Life" artist:@"No Doubt" coverUrl:@"https://nycavantgarde.files.wordpress.com/2012/07/no-doubt-no-doubt-65607_1024_768.jpg" year:@"2003"],
                                                  [[Album alloc] initWithTitle:@"Nothing Like The Sun" artist:@"Sting" coverUrl:@"http://kingofwallpapers.com/sting/sting-007.jpg" year:@"1999"],
                                                  [[Album alloc] initWithTitle:@"Staring at the Sun" artist:@"U2" coverUrl:@"http://fullhdpictures.com/wp-content/uploads/2015/12/U2-Wallpapers.jpg" year:@"2000"],
                                                  [[Album alloc] initWithTitle:@"American Pie" artist:@"Madonna" coverUrl:@"http://cdn.playbuzz.com/cdn/396371ea-919a-4c83-9e9d-d467d295010e/9473b53f-6092-49e5-83d5-6af5c43c18b5.jpg" year:@"2000"]]];
            [self saveAlbums];
        }
    }
    return self;
}

- (NSArray *)getAlbums{
    return albums;
}


-(void)saveAlbums{
    NSString *fileName = [NSHomeDirectory()stringByAppendingString:@"/Documents/albums.bin"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:albums];
    [data writeToFile:fileName atomically:YES];
    
}

- (void)addAlbum:(Album *)album atIndex:(int)index{
    if (albums.count >=index) {
        [albums insertObject:album atIndex:index];
    }else{
        [albums addObject:album];
    }
}

- (void)deleteAlbumAtIndex:(int)index{
    [albums removeObjectAtIndex:index];
}


-(void)saveImage:(UIImage *)image fileName:(NSString *)fileName{
    fileName = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@",fileName];
    NSData *data = UIImagePNGRepresentation(image);
    [data writeToFile:fileName atomically:YES];
}

-(UIImage*)getImage:(NSString *)fileName{
    fileName = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@",fileName];
    NSData *data = [NSData dataWithContentsOfFile:fileName];
    return [UIImage imageWithData:data];
}
@end
