#Patrones de diseño en IOS (IOS Design Patterns)
Articulo de rayWendelich
[https://www.raywenderlich.com/46988/ios-design-patterns](https://www.raywenderlich.com/46988/ios-design-patterns)


Los patrones de diseño son soluciones reutilizables para problemas comunes en el diseño de software.

Vamos a ponernos al dia sobre los principales [patrones de diseño](https://es.wikipedia.org/wiki/Patr%C3%B3n_de_dise%C3%B1o)

## Patrones de diseño comunes en Cocoa
* Creación: Singleton y Abstract Factory
* Estructurales: MVC, Decorator, Adapter, Facade y Composite
* Comportamiento(Behavioral) : Observer, Memento, Chain of Resposibilit Command


##MVC - El Rey de los patrones de diseño
Modelo vista controlador 

### ¿Cómo usar el Patron MVC?
Estructura de carpetas
![image](/estructuraCarpetas.png)

## Patron Singleton
El patrón singleton garantizar que una clase sólo tenga una instancia y proporcionar un punto de acceso global a ella.

###¿Cómo usar el patrón Sigleton
Creamos en nuestra clase un método que cree una instancia del objeto únicamente si no esta creada ya.



## Patrón Fachada(Facade)
Provee de una interfaz unificada simple para acceder a una interfaz o grupo de interfaces de un subsistema
Este patrón es ideal cuando trabajamos con muchas clases , particularmente cuando son dificiles de usar y comprender.


### Cómo utilizar el patrón Fachada(Facade)

## Patrón (Decorator)
Añade funcionalidad a una clase dinámicamente.
En objective-C tenemos dos formas de implementar este patrón: Categorias(Category) y delegación(Delegation)
###Categorias(Category)
Las categorias ayudan a mantener los metodos organizados y seperados en secciones
Es ligeramente 
#### Cómo utilizar Categorias
File\NEW\IOS\sources\Objective-C file
###Delegación (Delegation)
Es el mecanismo en el que un objeto actua en el nombre de otro o coordinadamente.
Es un patrón muy importante, Apple lo utiliza en muchas de las clases de UIKit

#### Cómo utilizar delegación

## Patrón (Adapter)
Permite que interfaces de clases incopatibles trabajen juntas.
###Cómo usar adapters
Creando una clase de objective Normal y añadiendo lo siguiente:
Em el .h

```
@protocol HorizontalScrollerDelegate;
@interface HorizontalScroller : UIView

@property (weak) id<HorizontalScrollerDelegate> delegate;
- (void)reload;

@end

@protocol HorizontalScrollerDelegate <NSObject>

@required

```

en el .m

```
#!objective-c

@interface HorizontalScroller() <UIScrollViewDelegate>


@end

@implementation HorizontalScroller{
 
    UIScrollView *scroller;    
}

```
##Patron (observer)
Comunicación entre objetos cuando hay algún cambio. <br>Fomentando la separación del código
La implementación suele requerir que el observador registre interes por otro objeto.

<br/>
Cocoa implementa el patrón observer de 2 formas diferentes:
###Notificaciones (Notifications)

No confundir con las notificaciones PUSH o las notificaciones locales.

#### ¿Cómo usar notificaciones?
En el objeto que queremos observar se implementa un postNotificationName

```
#!objective-c

[[NSNotificationCenter defaultCenter]postNotificationName:@"BLDownloadImageNotification" object:self userInfo:@{@"imageView":coverImage,@"coverUrl":albumCover}];
```

En el objeto que recibe la notificación se implementa: En este caso llama al método "downloadImage"

```
#!objective-c

[[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(downloadImage:) name:@"BLDownloadImageNotification" object:nil];
```
En la misma clase es importante eliminar el observador del objeto cuando el objeto ya no está en memoria si no, puede crashear la app.

```
#!objective-c

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
```

### Observador clave valor (Key-Value Observing (KVO))
El **KVO** permite observar cualquier cambio en una propiedad

####¿Cómo usar clave valor (Key-Value Observing (KVO))?
Añadimos un **Observer** a la propiedad que queremos y le ponemos una clave

```
#!objective-c

[coverImage addObserver:self forKeyPath:@"image" options:0 context:nil];

```

Es muy importante que nos acordemos de eliminar el observer cuando el objeto sea destruido

```
#!objective-c

-(void)dealloc{
    [coverImage removeObserver:self forKeyPath:@"image"];
}
```

Quien hace la magia? Pues tenemos que implementar un observadorç


```
#!objective-c

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"image"]) {
        [indicator stopAnimating];
    }
}
```

## Memento
Si cierras la app y la vuelves a abrir te darás cuenta que no se ha guardado la última sesión. ¿No sería genial que cuando abrieras la app, estuviera donde lo dejaste? De esto trata Memento

captura y externaliza el estado interno del objeto. Recupera la información sin violar la encapsulación. La información privada sigue siendo privada.

###¿Cómo utilizar Memento?
Añadimos estos métodos al final del ViewController.m


```
#!objective-c
#pragma mark - Restaurar último estado
/**
 *  @author DDura, 16-08-29 12:08:59
 *
 *  Si cierras la app y la vuelves a abrir te darás cuenta que no se ha guardado la última sesión. ¿No sería genial que cuando abrieras la app, estuviera donde lo dejaste? De esto trata Memento
 */

-(void)saveCurrentState{
    [[NSUserDefaults standardUserDefaults]setInteger:currentAlbumIndex forKey:@"currentAlbumIndex"];
    
}
-(void)loadPreviousState{
    currentAlbumIndex = [[NSUserDefaults standardUserDefaults]integerForKey:@"currentAlbumIndex"];
    [self showDataForAlbumAtIndex:currentAlbumIndex];
}
```

Cargar el estado anterior: Se hace desde el ViewController.(Antes de crear el **HorizontalScroller**)

```
#!objective-c
[self loadPreviousState];
```


Crear el disparador : También en el ViewController, pero al final

```
#!objective-c
[[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(saveCurrentState) name:UIApplicationDidEnterBackgroundNotification object:nil];
```

###Archiving 
Una de las implementaciones de Memento es Archiving. Convierte un objeto en una secuencia (stream) que puede ser guardado y cargado sin exponer las propiedades privadas a classes externas. Para más info ir a la docu oficial de [Apple](https://developer.apple.com/library/ios/documentation/Cocoa/Conceptual/Archiving/Archiving.html)

#### Cómo usar archiving
Primero hay que añadir el protocolo **NSCoding** en la clase en cuestion : Album.h

```
#!objective-c
@interface Album : NSObject <NSCoding>
```

Añadir los siguientes métodos a Album.m

```
#!objective-c
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.year forKey:@"year"];
    [aCoder encodeObject:self.title forKey:@"album"];
    [aCoder encodeObject:self.artist forKey:@"artist"];
    [aCoder encodeObject:self.coverUrl forKey:@"cover_url"];
    [aCoder encodeObject:self.genre forKey:@"genre"];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        _year       = [aDecoder decodeObjectForKey:@"year"];
        _title      = [aDecoder decodeObjectForKey:@"album"];
        _artist     = [aDecoder decodeObjectForKey:@"artist"];
        _coverUrl   = [aDecoder decodeObjectForKey:@"cover_url"];
        _genre      = [aDecoder decodeObjectForKey:@"genre"];
        
    }
    return self;
}
```

Ahora en la clase PersistencyManager.h debemos añadir el método : - (void)saveAlbums;

Mientras que en PersistencyManager.m añadimos

```
#!objective-c
-(void)saveAlbums{
    NSString *fileName = [NSHomeDirectory()stringByAppendingString:@"/Documents/albums.bin"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:albums];
    [data writeToFile:fileName atomically:YES];
    
}
```

y en el método init 

```
#!objective-c
NSData *data = [NSData dataWithContentsOfFile:[NSHomeDirectory()stringByAppendingString:@"/Documents/albums.bin"]];
        
albums = [NSKeyedUnarchiver unarchiveObjectWithData:data];
  if (albums == nil) {
      }
        
```

En libraryAPI.h

```
#!objective-c
- (void)saveAlbums;
```


En libraryAPI.m

```
#!objective-c
- (void)saveAlbums{
    [persistencyManager saveAlbums];
}
```

en el ViewController.m al final del método **saveCurrentState**

```
#!objective-c

[[LibraryAPI sharedInstance]saveAlbums];
```

##Patrón (command)
La opción "deshacer" es interesante mencionarla cuando vamos a implementar un "boton Eliminar"

###Cómo usar el patrón (Command)
Creamos la ToolBar con UIToolbar y un array para almacenar la cola de acciones.
En el ViewController.m

```
#!objective-c
    UIToolbar *toolbar;
    NSMutableArray *undoStack;
```


Creamos la barra al principio del viewDidLoad:

```
#!objective-c
    toolbar = [[UIToolbar alloc]init];
    UIBarButtonItem *undoItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemUndo target:self action:@selector(undoAction)];
    
    undoItem.enabled=NO;
    UIBarButtonItem *space =[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *delete = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteAlbum)];
    
    [toolbar setItems:@[undoItem,space,delete]];
    
    [self.view addSubview:toolbar];
    
    undoStack = [[NSMutableArray alloc]init];
```

Añadimos la vista en el método viewWillLayoutSubviews

```
#!objective-c
https://developer.apple.com/legacy/library/documentation/Cocoa/Conceptual/CocoaFundamentals/CocoaDesignPatterns/CocoaDesignPatterns.html#//apple_ref/doc/uid/TP40002974-CH6-SW32
```

Finalmente añadimos los métodos de añadir, eleminar y deshacer.


```
#!objective-c
- (void)addAlbum:(Album*)album atIndex:(int)index{
    [[LibraryAPI sharedInstance]addAlbum:album atIndex:index];
    currentAlbumIndex = index;
    [self reloadScroller];
}

- (void)deleteAlbum{
    Album *deleteAlbum = allAlbums[currentAlbumIndex];
    
    NSMethodSignature *sig = [self methodSignatureForSelector:@selector(addAlbum:atIndex:)];
    NSInvocation *undoAction = [NSInvocation invocationWithMethodSignature:sig];
    [undoAction setTarget:self];
    [undoAction setSelector:@selector(addAlbum:atIndex:)];
    [undoAction setArgument:&deleteAlbum atIndex:2];
    [undoAction setArgument:&currentAlbumIndex atIndex:3];
    [undoAction retainArguments];
    
    [undoStack addObject:undoAction];
    
    [[LibraryAPI sharedInstance]deleteAlbumAtIndex:currentAlbumIndex];
    [self reloadScroller];
    
    [toolbar.items[0]setEnabled:YES];
    
}


- (void)undoAction{
    if (undoStack.count > 0) {
        NSInvocation *undoAction = [undoStack lastObject];
        [undoStack removeLastObject];
        [undoAction invoke];
    }
    
    
}

```


##Patrón (Abtrsct Factory)

Más info en la [documentación de Apple](https://developer.apple.com/legacy/library/documentation/Cocoa/Conceptual/CocoaFundamentals/CocoaDesignPatterns/CocoaDesignPatterns.html#//apple_ref/doc/uid/TP40002974-CH6-SW32)

##Patrón (Chain of Responsability (aka Responder Chain))